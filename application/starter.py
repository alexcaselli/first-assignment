# This class initialize the database and the necessary tables
import MySQLdb

def inizializzaDB():
    # Connecting to database as root
    database = MySQLdb.connect("mysql","root", "password")
    cursor = database.cursor()

    # Print all the databases in the system (for debug purpose only, can be removed)
    cursor.execute('show databases;')
    for (databases) in cursor:
        print databases[0]

    # If the VOTI database doses not exist, create and open it (if already exist a warning will be generated)
    cursor.execute("CREATE DATABASE IF NOT EXISTS voti;")
    cursor.execute("use voti;")

    # If tables doses not exist, create it (if already exist a warning will be generated)
    cursor.execute("CREATE TABLE IF NOT EXISTS voti (matricola INT, voto INT, corso TEXT, data DATETIME, PRIMARY KEY(matricola, corso(40)))")