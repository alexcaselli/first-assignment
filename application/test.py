import functions
import starter

def test_check_mtr():
    mtr_valide = ["123", "32424333", "238882"]
    mtr_errate = ["abcd", "-90", "prova123prova"]

    for mtr in mtr_valide:
        try:
            assert functions.check_mtr(mtr) == True
        except AssertionError:
            print("Errore: " + mtr + " non e' stato accettato anche se nel formato corretto")
            quit()

    for mtr in mtr_errate:
        try:
            assert functions.check_mtr(mtr) == False
        except AssertionError:
            print("Errore: " + mtr + " e' stato accettato anche se e' nel formato NON corretto")
            quit()
            
def test_check_voti():
    voti_validi = [18, 25, 30]
    voti_errati = ["abcd", 10, 36]

    for voto in voti_validi:
        try:
            assert functions.check_voto(voto) == True
        except AssertionError:
            print("Errore: " + voto + " non e' stato accettato anche se nel formato corretto")
            quit()

    for voto in voti_errati:
        try:
            assert functions.check_voto(voto) == False
        except AssertionError:
            print("Errore: " + voto + " e' stato accettato anche se e' nel formato NON corretto")
            quit()
            
def test_check_corsi():
    corsi_validi = ["analisi", "sturo", "complementi"]
    corsi_errati = ["1", "-90", ""]

    for corso in corsi_validi:
        try:
            assert functions.check_corso(corso) == True
        except AssertionError:
            print("Errore: " + corso + " non e' stato accettato anche se nel formato corretto")
            quit()

    for corso in corsi_errati:
        try:
            assert functions.check_corso(corso) == False
        except AssertionError:
            print("Errore: " + corso + " e' stato accettato anche se e' nel formato NON corretto")
            quit()

def test_duplicate():
    starter.inizializzaDB()
    corso = "analisi"
    matricola = "807122"
    voto = 27
    try:
        assert functions.addRecord(voto, matricola, corso) == True
    except AssertionError:
        print("Errore: il record non e' stato inserito")
        quit()
    try:
        assert functions.addRecord(voto, matricola, corso) == "duplicate"
    except AssertionError:
        print("Errore: il record e' stato inserito comunque")

def test_replace():
    starter.inizializzaDB()
    corso = "analisi"
    matricola = "807122"
    voto = 30
    try:
        assert functions.replaceRecord(voto, matricola, corso) == True
    except AssertionError:
        print("Errore: il record non e' stato rimpiazzato")
        quit()
        

print("Inizio dei test\n")
test_check_mtr()
test_check_voti()
test_check_corsi()
test_duplicate()
test_replace()
print("Test superati")


