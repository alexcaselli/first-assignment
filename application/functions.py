import MySQLdb

def retCursor():
    database = MySQLdb.connect("mysql","root", "password")
    cursor = database.cursor()
    cursor.execute("use voti;")
    return cursor, database

def check_mtr(mat):
    if (mat.isdigit()):
        if(mat > 0):
            return True
    else:
        return False
        
def check_voto(voto):
    try:
        value=int(voto)
        if(int(voto) > 17 and int(voto)<=30):
            return True
        else:
            return False
        
    except ValueError:
        print("This is not a whole number.")
        return False

def check_corso(corso):
    if (len(corso) > 0 and corso.isalpha() == True):
        return True
    else:
        return False

def max_voto(corso):
    cursor, database=retCursor()
    cursor.execute("SELECT max(voto) FROM voti WHERE corso='%s'"%(corso))
    dbLine=cursor.fetchone()
    return dbLine[0]

def addRecord(voto, matricola, corso):
    cursor, database=retCursor()
    try:
        cursor.execute("INSERT INTO voti (matricola, voto, corso) VALUES (%s, %s, '%s');" %(matricola, voto, corso)) 
        database.commit()
    except MySQLdb.Error as e:
        database.rollback()
        error='Got Error {!r}, errno is {}'.format(e, e.args[0])
        if 'Duplicate' in error:
            return "duplicate"
        else:
            return error
    return True
    

def replaceRecord(voto, matricola, corso):
    cursor, database=retCursor()
    try:
        cursor.execute("REPLACE INTO voti (matricola, voto, corso) VALUES (%s, %s, '%s');" %(matricola, voto, corso))
        database.commit()
    except MySQLdb.Error as e:
        return error
    
    return True
            
