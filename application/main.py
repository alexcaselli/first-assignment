import starter
import functions
 

# Connecting to database as root
starter.inizializzaDB()

update='1'
matricola = raw_input('\n\nInserisci la tua matricola: ')
if(not functions.check_mtr(matricola)):
    print "\nERRORE formato matricola non valido\n"
else:
    voto = raw_input('Inserisci il tuo voto: ')
    if(not functions.check_voto(voto)):
        print "\nERRORE formato voto non valido\n"
    else:
        corso = raw_input('Inserisci nome del corso: ')
        if(not functions.check_corso(corso)):
            print "\nERRORE formato nome corso non valido\n"
        else:
            res=functions.addRecord(voto, matricola, corso)
            if res is not True:
                print res
                if 'duplicate' in res:
                    print "\n\nLo studente %s ha gia' un voto per il corso %s" %(matricola, corso)
                    update = raw_input('\n\nVuoi aggiornarlo? (S/N)')
                    update=update.lower()
                    
                    if update=='s':
                        functions.replaceRecord(voto, matricola, corso)
            
            print "\nVoto massimo nel DB per il corso %s: %s\n\n" %(corso, functions.max_voto(corso))



