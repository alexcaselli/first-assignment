FROM python:2.7


WORKDIR /myapp

RUN apt-get update
RUN apt-get install python-mysqldb
RUN pip install MySQL-python

COPY . /myapp

CMD ["python", "./application/test.py"]
