# First Assignment - DevOps

.First assignment PSW 2018 

* Alex Caselli (807129)
* Fabrizio Olivadese (820864)


**Link Repo**: https://gitlab.com/alexcaselli/first-assignment

## Applicazione

L'applicazione consiste in un programma Python che consente all'utente di registrare i voti ottenuti agli esami in un database MySQL.
L'applicazione ritorna poi il voto massimo presente nella tabella, avente come nome dell'esame il nome dell'esame appena inserito. Se lo studente ha giù un voto per il corso inserito viene chiesto se lo si vuole modificare o meno.
Sono presenti dei controlli sui campi da inserire, per garantire la consistenza e la correttezza dei dati inseriti.

Il programma utilizza un database relazionale MySql tramite la libreria mysqlDB di python.

## DevOps
Gli aspetti del paradigma DevOps che verranno sviluppati con questa applicazione sono:
* Containerizzazione/Virtualizzazione
* Integrazione e Sviluppo continuo CI/CD

Di seguito vengono analizzati:

## Containerizzazione/Virtualizzazione


<img src="img/docker.png" alt="docker" width="150"/>


Abbiamo deciso di realizzare **due** docker images, una contenente il programma python e i test e l'altra contenente il database Mysql alla versione 5.6

Quindi, le due immagini docker sono:
* Application: Un docker con immagine Python 2.7 che esegue il programma
* MySQL: il docker con immagine MySQL 5.6 che funge da database linkato con il docker principale

Il docker dell'applicazione viene eseguito linkando il docker del database, in modo che essi possano comunicare (vedi comandi successivi)

**Creazione delle immagini docker**
```
docker build -t first-assignement .     #Creazione del docker per l'esecuzione del programma python
docker run --name mysql -e MYSQL_ROOT_PASSWORD=password -d mysql:5.7 #Creazione/esecuzione del docker con il databse MYSQL, necessario al programma python
```
**Avvio delle immagini**
```
docker start mysql #ripresa dell'esecuzione dell'immagine docker MYSQL (se è stata stoppata precedentemente)
docker run -ti --link mysql first-assignement  #Esecuzion dell'immagine docker del programma python linkata all'immagine del database MYSQL (ti, per consentire input da terminale)
```

**Eliminazione immagini**
```
docker stop mysql
docker rm $(docker ps -aq)
```

## Integrazione continua:
Data la presenza del repository su GitLab, per questo aspetto utilizzeremo lo strumento CI/CD integrato di GitLab.
Abbiamo creato una classe test.py nella cartella applicazione che viene eseguita mediante GitLab grazie al file di configurazione .gitlab-ci.yml. Ad ogni commit vengono eseguiti i test con il relativo esito positivo o negativo.
Sono presenti due stage:
•	Build: crea ed esegue il push delle immagini docker 
•	Test: esegue le immagini docker (una con il database, l’altra contenente la classe di test)

## Sviluppo Continuo:
Per l’implementazione del CD è stata creata una macchina virtuale Ubuntu tramite il servizio cloud Google Cloud Platform nella quale effettuare il deploy.
Tramite ssh, abbiamo generato su quella macchina una coppia di chiavi rsa per poter consentire la comunicazione tra GitLab e la macchina.
La chiave privata e l’indirizzo IP della VM sono stati salvati tra le varibili messe a disposizione da GitLab, mentre la chiave pubblica è stata aggiunta tra le Deploy Keys di GitLab.
È stato generato in oltre un Deploy Token, tramite cui GitLab genera automaticamente delle variabili da utilizzare per l’accesso al registry.
Successivamente è stato aggiunto uno stage di deploy al file di configurazione .gitlab-ci.yml, il quale configura i parametri per la comunicazione tramite ssh in before-script e successivamente effettua il login al registry nella sezione script
Purtroppo, anche a causa di un prolungato bug di Gitlab che non ci ha consentito di inserire la deploy key per alcuni giorni non siamo riusciti ad implementare correttamente questo stage il quale verrà messo in commento nel file di configurazione.




## Conclusioni
Aspetti realizzati durante progetto:
•	Containerization con Docker
•	Continuous Integration con GitLab CI
•	Continuous Deployment con GitLab CI (in parte, vedi sopra)


